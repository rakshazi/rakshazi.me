---
title: "Nikita Chernyi"
hideComments: true
draft: false
---
**Senior Software Engineer** [PDF](/cv.pdf) [JSON](/cv.json)

Nikita Chernyi is a seasoned Senior Software Engineer with over a decade of backend development experience, specializing in Golang. He has a proven track record in building scalable, high-performance systems and optimizing cloud infrastructure for cost-efficiency. With a strong background in DevOps, Nikita has architected and maintained numerous microservices and high-traffic systems across industries like e-commerce, communications, and media.

As the founder of etke.cc, Nikita is a dedicated open-source contributor, particularly in the Golang ecosystem, with projects focused on Ansible tools, Matrix-related services, and automation. His expertise spans from backend development to production deployment, with a consistent focus on delivering robust, scalable, and business-driven solutions.

> **Contacts**
>
> [cv@rakshazi.me](mailto:cv@rakshazi.me) | [@aine:etke.cc](https://matrix.to/#/@aine:etke.cc) | [linkedin](https://www.linkedin.com/in/rakshazi/) | [blog](https://rakshazi.me) | [github](https://github.com/aine-etke) ( [old](https://github.com/rakshazi)) | [gitlab](https://gitlab.com/rakshazi)

**Areas of Expertise**

`Software Development Lifecycle` `Backend Applications Design & Development` `Techincal Process Improvement`

**Technical Skills**

`Go`, `Docker`, `AWS`, `Linux`, `Ansible`, `GitLab`, `GitHub`, `CircleCI`, `Jenkins`, `Python` (scripting), `Bash` (scripting)

**Table of Contents**

{{< toc >}}

## Work Experience

### 2021 - Now: Founder @ etke.cc

**Founder | [etke.cc](https://etke.cc) | Remote**

During the past years, I have spearheaded the establishment and growth of etke.cc, a managed Matrix hosting service that empowers individuals and organizations to host the open Matrix platform in an open and no-vendor-lock-in way.
As the Founder, I have been instrumental in shaping the service's direction, driving innovation, and fostering a collaborative and open-source culture.

Key Accomplishments:

* Successfully launched the etke.cc managed Matrix hosting service, providing users with a reliable and customizable platform for their communication needs.
* Led a team in pushing hundreds updates and enhancements to the automation framework, the core of our service, ensuring its continuous improvement and stability.
* Integrated dozens of components into the matrix stack, expanding the functionality and versatility of our services.
* Developed plenty of bots and tools that extended matrix capabilities, enabling users to fully leverage the power of the platform.
* Provided assistance to hundreds individuals and organizations, supporting them in achieving their goals in the matrix ecosystem.
* Built an effective customer support tool chain across different communication channels and decreased average time to resolution of customers’ requests to a matter of minutes

Milestones and Highlights:

* Started the project in February 2021, with the vision of creating a comprehensive and user-centric matrix hosting service.
* Pioneered the installation of etke.cc as the first server, establishing a strong foundation for the service.
* Developed a chatbot server as the second installation, leveraging matrix as a platform to interact with users across different chat networks.
* Expanded our services globally in May 2021, marking a significant milestone in our growth and reach.
* Developed the Scheduler, a major service that allows customers to manage their Matrix servers more efficiently. It enables automated maintenance, service restarts, and disk usage checks through chat interactions, providing users with greater control and convenience.
* Implemented the internal etke.cc container registry mirror, ensuring a stable and efficient maintenance process by eliminating downtime issues associated with external registries.
* Developed an internal monitoring system powered by Prometheus and Grafana, proactively detecting issues and minimizing server outages.
* Integrated alerting functionality, enabling timely notifications for critical events and facilitating proactive actions to mitigate potential downtime.
* Promoted a spirit of openness by releasing all work as free software in git repositories.

As the Founder of etke.cc, I have demonstrated exceptional leadership, technical expertise, and a deep commitment to providing reliable and innovative solutions to our users. By driving the service's vision, fostering collaboration, and contributing to the open-source community, I have positioned etke.cc as a trusted and forward-thinking player in the Matrix ecosystem.

### 2024 - Now: Go Developer @ Hellotickets

**Go Developer | [Hellotickets](https://hellotickets.com) | Remote**

Making sure your payments don’t disappear into the void at Hellotickets - stay tuned, it's still brewing!

### 2021 - 2023: Senior Software Engineer @ Crunchyroll

**Senior Software Engineer | [Crunchyroll](https://crunchyroll.com) | Chisinau, Moldova**

* Successfully integrated content ratings enhancements into data pipeline, improving user engagement and providing valuable information for Crunchyroll users
* Spearheaded ElasticSearch stability improvements, implementing a robust solution that ensured smooth operations by proper horizontal scaling and smart ingestion.
* Collaborated with the Content Management team to integrate Sony Music content into the content discovery and search mechanisms, expanding the platform's library and enhancing the overall user experience.
* Developed and implemented Dub Rendition functionality in the search and content discovery mechanisms, allowing users to easily explore and enjoy dubbed content.
* Integrated real-time recommendations into the content discovery mechanisms, leveraging machine learning algorithms to deliver personalized content suggestions to users in real-time.
* Incorporated an A/B testing platform into the content discovery mechanisms, enabling data-driven decision-making and optimizing user engagement and conversion rates.
* Optimized the caching layer of backend applications, resulting in increased throughput and a significant reduction in the required number of servers, leading to cost savings and improved system performance.
* Proactively conducted vulnerabilities scanning, performed Continuous Integration migrations, and integrated linters, ensuring the robustness, quality and security of the backend applications.
* Implemented Discord Rich Presence integration, enhancing the user experience and providing real-time presence updates within the application.
* Contributed to the development of the backend service for the Home Page, increasing engagement, enabling efficient and seamless navigation for users.
* Implemented various search mechanism optimizations, improving search speed, accuracy, and relevance of results.
* Strengthened the security of backend applications by implementing robust security measures and best practices, ensuring the confidentiality, integrity, and availability of platform's data.

During my tenure at Crunchyroll, I consistently demonstrated strong technical expertise and a proactive approach to problem-solving, resulting in successful project deliveries and improved system performance.

### 2017 - 2021: Senior Backend Developer @ Titanium Software

**Senior Backend Developer, DevOps | [Titanium Software](https://titanium.codes) | Chisinau, Moldova**

As a Senior Backend Developer at Titanium Software, a leading technology company, I played a pivotal role in developing and supporting custom solutions for a diverse range of clients, from startups to mature businesses.
With a focus on ensuring scalability, performance, and reliability, I actively contributed to the growth and success of the company.

Key Responsibilities and Achievements:

* Integrated best practices of "Infrastructure as Code" to streamline and automate the deployment and management of software systems.
* Championed the "DevOps way" throughout the organization, promoting collaboration, automation, and continuous improvement across all areas of the company.
* Implemented LDAP as the primary identity management tool, enabling efficient user authentication and access control.
* Integrated a multitude of code-quality analytic services and tools, ensuring adherence to coding standards and enhancing the overall quality of software solutions.
* Played a key role in building company processes and workflows, establishing efficient and scalable development practices.
* Made significant contributions to the open-source community, sharing code, insights, and best practices with the broader developer community.
* Led a team in five projects, providing technical guidance, mentorship, and ensuring successful project delivery.
* Contributed to the development of backend architectures, designing robust and scalable systems that met the specific needs of clients.

Throughout my tenure at Titanium Software, I consistently demonstrated a deep understanding of backend development principles, DevOps methodologies, and architectural design.
By leveraging my technical expertise and leadership skills, I contributed to the success of numerous projects while driving innovation and excellence in software development.

### 2014 - 2017: Backend Developer @ OpsWay

**Backend Developer, DevOps | [OpsWay](https://opsway.com) | Remote**

As a dedicated Backend Developer specializing in e-commerce solutions, I have successfully developed and supported various projects based on Magento Community Edition and Enterprise Edition.
With a strong focus on delivering robust and efficient integrations, I have contributed to the success of numerous e-commerce platforms.

Key Responsibilities and Achievements:

* Developed and maintained over 10 full-featured Magento extensions, ensuring compatibility with both the Community and Enterprise editions. These extensions enhanced the functionality and performance of the e-commerce platforms.
* Participated in the development of more than 10 production-ready products, employing a fully dockerized approach. This allowed for efficient product life cycle management within Docker containers, ensuring scalability, flexibility, and ease of deployment.
* Managed multiple projects using Ansible, a powerful configuration management tool. Leveraging Ansible, I successfully handled various aspects of project management, including configuration management, network management, package management, data management, continuous integration (CI), and deployment.
* Implemented efficient processes and workflows that reduced new instance preparation time for production to only 10-15 minutes. This resulted in quicker deployment and enhanced productivity.
* Made significant contributions to customers' products, providing over 1000 contributions per year. These contributions included bug fixes, feature enhancements, and optimizations, ultimately improving the quality and performance of the e-commerce solutions.

Throughout my experience as a Backend Developer, I have demonstrated a strong understanding of e-commerce platforms, particularly Magento, and have consistently delivered high-quality solutions. 
With expertise in containerization using Docker and proficiency in configuration management with Ansible, I have successfully developed and maintained robust and scalable e-commerce applications. 
My commitment to continuous improvement and my ability to contribute significantly to customers' products have played a key role in achieving their business objectives.

### 2013 - 2014: Support Engineer @ Active Computers

**Support Engineer | [Active Computers](https://www.acomps.ru) | Tiraspol, Moldova**

As a Support Engineer at Active Computers, I played a vital role in providing exceptional support and development services for a variety of websites.
With a focus on delivering excellent customer service and technical expertise, I ensured the smooth operation and continuous improvement of online stores, business sites, and a custom web-based issue tracking system for internal usage.

Key Responsibilities and Achievements:

* Provided technical support to clients, addressing their inquiries and resolving issues promptly and effectively. This included troubleshooting website functionality, resolving performance issues, and assisting with general inquiries related to online stores and business sites management.
* Collaborated with cross-functional teams to analyze and diagnose complex technical problems, implementing effective solutions to ensure optimal website performance and user experience.
* Actively participated in the development and enhancement of multiple websites, leveraging my technical skills to implement new features, improve functionality, and optimize performance.
* Played a key role in the development and maintenance of a custom web-based issue tracking system. This system streamlined internal processes and facilitated efficient communication and problem resolution within the organization.
* Demonstrated exceptional problem-solving skills and attention to detail in investigating and resolving customer-reported issues, ensuring a high level of customer satisfaction.
* Developed strong relationships with clients, fostering trust and effectively managing customer expectations.
* Collaborated closely with the operations team, providing valuable feedback and insights from customer interactions to drive continuous improvement in website performance and user experience.

Throughout my tenure as a Support Engineer, I consistently delivered excellent customer service and technical support, contributing to the success and functionality of various websites. My strong troubleshooting skills, attention to detail, and collaborative approach were instrumental in resolving complex technical issues and providing effective solutions. By actively participating in the development process, I contributed to the improvement of website functionality and user satisfaction.

## Projects

### etke.cc

#### MatrixRooms.info

Fully-featured search engine for Matrix rooms, discovering content over federation.
Participating in all processes, starting from the initial website design to customer support.

[Website](https://matrixrooms.info)

### Titanium Software

#### Tpoint

A mobile application for Squash players, that helps them book a game in a Tpoint court,
control the smart devices inside the court, and share the live video of their game, and its score.
The app is substituting the human factor in the process of reserving, paying,
customizing the court settings, and playing like a social media platform for Squash players.

[Website](https://tpoint.club)

#### 2CB

A B2B online portal that works by getting both sides of a call to dial into a central point using local access numbers.
The key feature is taking a call from the calling party and using this as a trigger to automate a call into the 2CB system from the person being called.
The person being called will ideally have the same experience as a regular inbound call.

#### An IoT project

Role: DevOps

Used tools:

* Azure and AWS as cloud providers
* Kubernetes as cluster management
* Docker container as an infrastructure unit
* Ansible as configuration and provision management of hosts
* Couchbase as main DB storage
* Elasticsearch as a search engine
* Kafka + Zookeeper as stream collector and message queue
* ... and much more things to build a robust solution for IoT.

#### MathodiX

Role: DevOps

* Integrated autoscaling for High Availability
* Integrated Sentry for crash reporting and handling

[Website](https://mathodix.com)

#### SportsWith

A project built to provide sports events-sharing service

Role: Project lead, DevOps, Backend Developer

Backend development:

* Highly optimized backend, built on PHP7.1 and Slim Framework
* Image optimizations, based on user metadata (supported formats, device screen sizes, used browsers, etc.)
* Social authorization
* Bonus program, based on social activity
* Live discussion on virtual events
* ... and much more

DevOps:

* Fully dockerized infrastructure
* Implemented pattern "Infrastructure as a Code"
* Implemented full Continuous Integration and Continuous Delivery pipelines
* ... and much more

#### fone.do

DevOps:

* support existing project infrastructure and automatizations

[Website](https://fonedo.bpo.do/)

### OpsWay

#### Antoshka

Backend development:

* Deep store integration with NovaPoshta shipping service with full process automation
* Deep highly customizable fully automated integration with SMS gateways (TurboSMS.ua, GMS-Worldwide.com)
* High-customizable export system for 10+ price aggregators with multiple export formats
* Custom event system - deep integration with Magento Cron and shell scripts with full control from the Magento admin control panel and providing additional data for store managers and developers
* Lots of Magento custom logic improvements

DevOps:

* Fully managed via Ansible - configuration, data, packages, backup management; continuous integration, deployment, and rollback via ansistrano
* Fully dockerized (all app components live in separate docker containers)
* Decomposed back office and frontend parts of Magento on separate nodes (managed via Ansible)

[Website](https://antoshka.ua)

### Open Source

_previously this section contained list of projects with links to their repositories, but due to the high amount of projects, changes were made_

I devote a significant amount of spare time to open-source projects, contributing to various tools, libraries, and frameworks.
As a founder of etke.cc, almost every Go open-source project I work on is available on
[github.com/etkecc](https://github.com/etkecc), and related to the following topics:

- [Ansible-related tools](https://github.com/search?q=topic%3Aansible+org%3Aetkecc+fork%3Atrue&type=repositories)
- [Matrix-related bots, tools, and services](https://github.com/search?q=topic%3Amatrix-org+org%3Aetkecc+fork%3Atrue&type=repositories)
- [Various Go libraries](https://github.com/search?q=topic%3Alibrary+org%3Aetkecc+fork%3Atrue&type=repositories)


Additionally, there are numerous of ansible roles, located at [github.com/mother-of-all-self-hosting](https://github.com/mother-of-all-self-hosting)

## References

> Nikita is a good backend developer and DevOps engineer who is not afraid to jump into learning about new protocols and technologies. His work on https://etke.cc (and not only) demonstrates that he's capable of taking a problem and building a solution which spans the whole stack (business requirements, frontend/backend development, production deployment and customer support).
>
> -- **Slavi Pantaleev, Co-Founder, etke.cc**

> Nichita is a strong and passionate backend engineer, with lots of focus on improving the performance and a healthy state of the services he's working on. Along his time in Crunchyroll, his main focus was on building stable, scalable and performant backend services, that can handle a load of millions of requests per minute.
>
> In terms of optimising and scaling the infrastructure, one goal that Nichita is always keeping in mind are the costs of our services, optimising not only how they perform, but also how costly they would be in the end. Every decision he makes in the development process, he would reiterate through all of the before mentioned standards for a microservice.
>
> Besides building services, Nichita likes to delve deeper into the Golang architecture and understanding the principles of the language, so that we would follow the latest releases and use all the powerful tools Golang gives us to achieve the end goal.
>
> -- **Vlad Ledniov, Engineering Manager, Crunchyroll**

> Nikita is a very good developer with a lot of knowlegdes in different domains.
>
> -- **Leahu Ion, Frontend Developer, Titanium Soft**
>
>
> In the most scenarios Nikita Chernyi knows how to fix an issue that he encountered, due to his rich experience with different programming language, systems and frameworks.
> In my professional career, I've never met someone with this amount of knowledge in creating a secure system on a local environment or a cloud service like AWS, Azure, Heitzner, DigitalOcean, etc. as well as creating the required architecture on any provider described before.
> He proved his deep knowledge with nginx and kubernetes alike services (creating auto scaling groups, load balancers, docker images, certificates, etc.).
> Besides the capabilities described in DevOps domain, Nikita Chernyi has a big amount of experience with different programming languages like PHP, Go, JavaScript and Python, that he manifested by creating complex architectures from scratch, using best practices and required design patterns, up to fixing small issue in foreign projects.
> Nikita Chernyi has a big amount of repositories (that he is proud of, by showing them to others) created from scratch or other ones that were cloned with the purpose of helping the creators.
> In his free time he likes to read different articles from programming world and share them with his colleagues.
> Overall feedback: Nikita is a cool guy, as long as you don't mention that JavaScript is a cool language, because you can summon the Kraken by doing so.
>
> -- **Alexandru Scripnic, FullStack JavaScript Developer and Team Leader, Titanium Soft**
>
>
> Great php developer is the phrase that comes to mind when I think about Nikita Chernyi.
> I’ve had the pleasure of knowing Nikita Chernyi for six months, during which made server side architecture.
> Above all, I was impressed with Nikita Chernyi’s ability to complex solutions for server side via cloud technologies.
> And, of course, his  intelligent, reliable, helpful.
> Nikita Chernyi would be a true asset for any positions requiring responsibility and comes with my heartfelt recommendation.
>
> -- **Evghenii Covali, iOS Developer, Titanium Soft**
>
>
> He can do everything.
> Just give him task and it will be ready in best condition!
> After it you can drink some beer and eat pizza together.
> Long story short - nice guy and awesome professional!
>
> -- **Andrew Tomash, Android Developer, Titanium Soft**

> Really valuable team player, constantly researching technologies, he was the main driver to all key changes to our projects and development processes.
>
> -- **Andrew Taran, Project Manager, OpsWay**
>
>
> Nikita was working as remote developer in our team.
> For remote developers is really important to be able to have strong self-management and self-motivation skills.
> And Nikita is one of the best persons in this area for sure.
> He was always available in dozens of messengers, was able to to act very fast when it needed on support issues.
> If you're looking for good Magento Developer with deep DevOps skills - Nikita is a right choice.
> Waiting him back to OpsWay :)
>
> -- **Serghey Morin, Founding Partner, OpsWay**
>
>
> Nikita is a hight level developer with a big passion to building a good software.
> He knows what business want.
> Nikita helped our project to grow up.
>
> -- **Alexander Tolkach, Project Manager** (OpsWay's customer)
>
>
> I worked with him and giving consulting on DevOps & PHP developing.
> He is very quickly studying and is able to generate its own unique ideas.
> I recommend Nikita as responsible man & ability to constantly growth further.
>
> -- **Alexandr Vronskiy, CTO, OpsWay**
>
>
> Nikita is very active and hard-working employee.
> He is a great team player who enjoys learning new technologies.
> I believe he has lots of great start-up ideas and also he is smart enough to know how to implement them.
> He is an excellent worker since the day he came at our company to work as a PHP developer.
> He is a helpful person and very friendly :)
>
> -- **Iia Mizina, HR, OpsWay**
>
>
> Nikita is a great developer and very good DevOps.
> He's very goal-oriented.
> The developing and system administration are his passions.
> He cares about code quality and system stability.
> I haven't met anyone who knows more about Linux and different technologies related to it.
> It was a great pleasure to work with Nikita.
>
> -- **Volodymyr Rudakov, Software Engineer, OpsWay**
>
>
> He is experienced, creative and an excellent developer.
> I was glad for opportunity to work with him in one team.
>
> -- **Yuriy Kobrynyuk, Software Engineer, OpsWay**


> Nikita is very energetic and full of innovative ideas.
> He has great development and infrastructure management skills which help his clients to grow their business.
> Appreciate to work with him
>
> -- **Andrew, Project Manager, Active Computers**
