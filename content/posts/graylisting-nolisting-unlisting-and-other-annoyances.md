+++
title = "Graylisting, Nolisting, Unlisting, and Other Annoyances"
date = "2022-11-13T22:38:40+02:00"
author = "rakshazi"
authorTwitter = "" #do not include @
cover = ""
categories = ["smtp"]
tags = ["smtp", "project::postmoogle"]
keywords = ["smtp", "email", "composing", "headers"]
description = "In [one of the previous posts](/posts/how-to-validate-email-address/) I mentioned a problem called `greylisting`. Let's discuss it a bit further and add other annoying and shady anti-spam techniques that add headache and not protect you from spam."
showFullContent = false
readingTime = false
hideComments = false
+++

Hey,

This post is a bit more about citations from other sources with my comments on the topic because the sources provide a pretty clear description of what each topic is and why it was implemented. From my side, I'd add why I hate each topic but have to reconcile with them.
Let's roll!

## greylisting

> Greylisting is a new method of blocking significant amounts of spam at the mailserver level, but without resorting to heavyweight statistical analysis or other heuristical (and error-prone) approaches. Consequently, implementations are fairly lightweight, and may even decrease network traffic and processor load on your mailserver.
>
> Greylisting relies on the fact that most spam sources do not behave in the same way as "normal" mail systems. Although it is currently very effective by itself, it will perform best when it is used in conjunction with other forms of spam prevention. For a detailed description of the method, see the [Whitepaper](http://projects.puremagic.com/greylisting/whitepaper.html).
>
> The term _Greylisting_ is meant to describe a general method of blocking spam based on the behavior of the sending server, rather than the content of the messages. Greylisting does not refer to any particular implementation of these methods. Consequently, there is no single Greylisting product. Instead, there are many products that incorporate some or all of the methods described here.
>
> -- [Evan Harris](http://projects.puremagic.com/greylisting/)

Greylisting means that whenever you send an email to an email server, it returns a 4xx error first and you have to re-send the same email a bit later. And that `a bit later` part is funny enough - you don't know what is the rate limit period and [RFC5321](https://datatracker.ietf.org/doc/html/rfc5321) states that it `SHOULD be at least 30 minutes` and `the give-up time generally needs to be at least 4-5 days`.

You **have to** keep an email in a queue for **at least** 4-5 days. That's crazy! And what's even crazier - people **do** that.
If you read previous posts in that blog, I already mentioned in [one of them](/posts/how-to-validate-email-address/) that I decided that greylisted request == invalid (nonexistent) email, period. I don't care who you are and why you use (or even configured) your email server that way - if you want to receive emails from me, please, make your mail server work as expected, because I don't have spare compute resources to deal with your shady spam protection techniques. If you don't like my email - say why in the error message.

Hmm... maybe I'll do the same thing - if an email server implies greylisting, I'll do the same in response to the `MAIL` command someday, just to show how that feels. Enough rants, let's move on.

## nolisting

> Poor Man's Greylisting 
>
> What is it?
> Nolisting reduces the amount of fire-and-forget spam an MTA must process by specifying a primary MX that is always unavailable.
> 
> Is it the same as greylisting?
> No, but it exploits the same noncompliant behaviour of spamware and viruses, including those that spread via internal SMTP engines.
>
> How does Nolisting work? 
> It has been observed that when a domain has both a primary (high priority, low number) and a secondary (low priority, high number) MX record configured in DNS, overall SMTP connections will decrease when the primary MX is unavailable. This decrease is unexpected because RFC 2821 (Simple Mail Transfer Protocol) specifies that a client MUST try and retry each MX address in order, and SHOULD try at least two addresses. It turns out that nearly all violations of this specification serve the purpose of sending spam or viruses. Nolisting takes advantage of this behaviour by configuring a domain's primary MX record to use an IP address that does not have an active service listening on SMTP port 25. RFC-compliant clients will retry delivery to the secondary MX, which is configured to serve the role normally performed by the primary MX (final delivery, transport rerouting, etc.).
>
> -- [nolisting.org](http://nolisting.org/)

So, the Nolisting technique forces you to try multiple SMTP servers from MX records when one of them fails.
To be honest, I even like the approach - almost any tool for interacting with SMTP does that by default and there is no additional implementation cost because you don't need to have an outgoing email in a queue for `at least 4-5 days`.

It's shady, it's annoying, but it's fairer than Greylisting because you know for sure that if a connection to an SMTP server failed - it failed, you don't need to guess "hm, may be if I try later it will work? How much later?".

## unlisting

And to justify the "unlisting and other annoyances" in the title here is one wild and dangerous (don't do that, really. Even if you implement Graylisting, don't implement that thing!) technique, not recommended even by its developers.

> Port Knocking for SMTP 
>
> What is it?
> Unlisting reduces the amount of fire-and-forget spam an MTA must process by using a packet filter to block access to all MX hosts unless a connection is attempted to the primary MX first. If a host retries the secondary MX from the same IP address within a specified period, the connection is allowed.
>
> Is it the same as greylisting?
> No, but it exploits the same noncompliant behaviour of spamware and viruses, such as those that spread via internal SMTP engines.
>
> Is it the same as Nolisting? 
> It includes all of the benefits of Nolisting, extending spam prevention to the functioning MX through the use of a packet filter. Nolisting should be fully understood and evaluated before attempting to use Unlisting.
>
> How does Unlisting work? 
>
> In [5. Address Resolution and Mail Handling, RFC 2821: Simple Mail Transfer Protocol (SMTP)](http://tools.ietf.org/html/rfc2821#section-5) specifies client behaviour concerning MX lookups
> If a host connects to a secondary MX without trying the primary, it is highly likely that it is attempting to deliver spam by bypassing the traditionally stricter filters found on primary MX hosts. 
> 
> By firewalling both MX hosts, a special flag can be set to allow the SMTP client to connect to the secondary MX for a specified time period if the primary MX was tried first and the client connection originates from the same IP address. The secondary MX is configured to handle final delivery, transport rerouting, etc.
> 
> Put simply, Unlisting is port knocking for SMTP.
>
> -- [nolisting.org](http://nolisting.org/unlisting.html)

I don't even know what I can add as a comment here - just read the source and make the `(*O*)` face.

## more information

It's not about "annoyances", but about some other tricks (sometimes wild) you may be interested in:

[The SpamAssasin's wiki page "Other Tricks"](https://cwiki.apache.org/confluence/display/spamassassin/OtherTricks)
