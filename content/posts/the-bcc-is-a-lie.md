+++
title = "The BCC Is a Lie"
date = "2022-11-19T23:15:50+02:00"
author = "rakshazi"
cover = ""
categories = ["smtp"]
tags = ["smtp", "project::postmoogle"]
keywords = ["smtp", "email", "composing", "headers"]
description = "I continue studying of SMTP and how email works and found out a big lie of 20th and 21st centuries! BCC doesn't exist."
showFullContent = false
readingTime = false
hideComments = false
+++

Hey,
[Postmoogle](https://gitlab.com/etke.cc/postmoogle) gets new features and bug fixes, while I get new knowledge,
sometimes rather hilarious one. As you know, that whole blog is about "raskhazi finds out things and tries to tuck them into the head", so today a new thing "clicked" in my mind - the BCC (blind carbon copy), the field in an email when you want to hide recipients, is not a header.

What's the problem, you may ask? Well, the fact that it's just not a header is odd itself, but in fact, it's a delta between headers and SMTP command(-s). I see the "wat" look on your face, just hear me out!

First of all, to understand what's going on we need to be on the same page about how "send email to somebody@example.com" works on example.com's SMTP server side.
To make the magic happen (and by magic I mean the lots of different hidden things between the "Send Email" button and "1 new email" notification), there are lots of steps and technologies involved, I can't even link to a specific post, so [just check the whole category](/categories/smtp/) to get some clues).

One of the parts of that process is communication between your email provider's MTA and the recipient email provider's MTA over SMTP that contains a bunch of commands, the most notable for our today's topic are:

* `RCPT TO` - the email address of the recipient of the email you trying to send. It must be 1 email address, but you may call that command multiple times
* `DATA` - the email itself (with headers and content)

As you may know, in an email there are headers and the content itself, and in the headers part you may have `To`, `Subject`, `Message-Id`, `In-Reply-To`, `Cc`, `References`, etc. 
Do you know what you can't find in email headers? **BCC**.

## Where is BCC?!

Nowhere. Literally. It's not a thing. It does not exist. The BCC is a lie.

**What** is BCC? That's the correct question!
BCC is a delta between the SMTP command `RCPT TO` and email headers `To` + `Cc`.
So, if `RCPT TO` has `somebody@example.org`, `To` has `someone@example.org`, and `Cc` has `someother@example.com` the BCC (at least, one of the recipients in it) will be `somebody@example.org`.

So, the result is a difference (a missing element) between `To+Cc` and `RCPT TO` (in our example: `someone@example.org+someother@example.com` and `somebody@example.org` = `somebody@example.org`).

Sometimes the existence of BCC is marked by `undisclosed-recipients:;` in email headers, but to understand that some particular mailbox is part of the BCC list, you need to find the difference between the mentioned above email headers and the SMTP command.

### What about CC?

> CC stands for Carbon Copy

It's an email header, why? Do you want to know if you can find out if CC was used even without the `Cc` header in an email?
Well, of course, you can!

If `somebody@example.org` is part of `Cc`, the SMTP `RCPT TO` command should be sent to the `example.org` server with that email as well (e.g.: `RCPT TO somebody@example.org`), while the `To` email header will have a different value (e.g.: `To: someone@example.org`).

Wait. Does that mean you can send an email where `To`, `Cc` (and any other header) contains absolutely unrelated data?
Yup. Nothing stops you from doing that ¯\_(ツ)_/¯
