+++
title = "DNS Block TikTok"
date = "2023-09-17T12:56:50+03:00"
author = "rakshazi"
cover = ""
categories = ["dns"]
tags = ["dns", "personal"]
keywords = ["dns", "personal"]
description = "I tried to block TikTok for my kid using [NextDNS](https://nextdns.io/?from=94mbn843) Parental Control, but that didn't work out. In this post I will reveal the mystery of the blocking TikTok using DNS blocklist"
showFullContent = false
readingTime = false
hideComments = false
+++

> In this post I will reveal the mystery of the blocking TikTok using DNS blocklist

mYsTeRy, huh. Nah, I'm kidding. That post is my personal note just to avoid loosing that info, because I spent 10 minutes figuring that out.

Here is the "mystery" - just block the following root domains and you're good:

```
*.tiktok.com
*.ttlivecdn.com
*.tiktokcdn-us.com
*.tiktokv.com
*.tiktokcdn.com
```

Apparently, NextDNS Parental Control doesn't catch them for some reason and there are multiple threads on their help forum related to that issue.

Nevertheless, I'm quite happy with their service - like dnsmasq on steroids and without self-hosting issues.
Here is my ref link, if you're interested: https://nextdns.io/?from=94mbn843 (hey, I do pay them money, so it's not an ad)
