---
title: "Hello, world!"
date: 2022-11-10T15:33:32+02:00
author: rakshazi
draft: false
---

Hola,

That blog is intended for personal quick notes about my projects and TIL (_today I learned_) things,
kinda personal knowledge base.

If you're looking for a CV - there you go: [rakshazi.me/cv](/cv).
