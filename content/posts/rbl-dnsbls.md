+++
title = "Real-Time Blackhole List (RBL) and DNS Blocklists (DNSBL) for email antispam"
date = "2024-07-30T12:56:50+03:00"
author = "rakshazi"
cover = ""
categories = ["dns", "smtp"]
tags = ["dns", "smtp", "project::postmoogle"]
keywords = ["dns", "smtp", "email", "antispam"]
description = "Continuation of the email antispam series, but this time I'll explain what is Real-Time Blackhole List (RBL) and how it is used with DNS Blocklists (DNSBL)"
showFullContent = false
readingTime = false
hideComments = false
+++

Buna ziua, dragi cititori!
I continue my journey on building [Postmoogle](https://gitlab.com/etke.cc/postmoogle) - an SMTP server with [Matrix](https://matrix.org) interface we use at [etke.cc](https://etke.cc) for email communications.
After discovering [Greylisting, Nolisting, Unlisting, and Other Annoyances](/posts/graylisting-nolisting-unlisting-and-other-annoyances/),
and [Forward Confirmed reverse DNS](/posts/dns-rdns-fcrdns/), I decided to dive further into the huge hole of email antispam techniques.

Today's topic is Real-Time Blackhole List (RBL) and DNS Blocklists (DNSBL).


So, let's start with

## Wat?

_I love memes, okay?_

Basically, every computer connected to the internet has an IP address. When a computer (server, virtual machine, etc.) is sending spam, recipients' servers can report that IP to a blacklist. When that IP will will try to send email again (even if that IP was released and given to a different server), the recipient's server will check the IP against the blacklist and reject the email if the IP is listed.

So, a list with such IPs is called DNS Blocklist (DNSBL), because it allows querying the list via DNS, and the process of checking the IP against the list is called Real-Time Blackhole List check (RBL), because you are supposed to query the list in real-time when a connection is established.

## Why?

Because spam. Allmost [all posts in the SMTP topic of this blog is about fightning spam](/categories/smtp/).
Because 99.99% of email traffic is clearly spam and only 0.01% is _possible_ legit email. And even that 0.01% is still questionable.

## How?

There are a lot of DNSBLs, for [Postmoogle](https://gitlab.com/etke.cc/postmoogle) we started with the following ones:

- `b.barracudacentral.org` - Barracuda Networks
- `bl.spamcop.net` - SpamCop
- `ix.dnsbl.manitu.net` - Manitu
- `psbl.surriel.com` - PSBL
- `rbl.interserver.net` - InterServer
- `spam.dnsbl.anonmails.de` - AnonMails
- `zen.spamhaus.org` - Spamhaus
- `rbl.your-server.de` - Hetzner

You can use any other, if you wish to. The process is simple - when you receive an email, you check the sender's IP against the DNSBLs you have configured. If the IP is listed in any of the lists, you reject the email.

To check an IP against a DNSBL, you need to perform a DNS query with the IP reversed and appended to the DNSBL domain. For example, to check `127.0.0.2` (which is a reserved IP for testing purposes, and all DNSBLs should list it as spam) against `zen.spamhaus.org`, you need to query `2.0.0.127.zen.spamhaus.org`. If the query returns a "listed" signal, you can reject the email.

Signal is a special IP address, in case of Spamhaus the list of interesting (for this topic) signals are:

- 127.0.0.2
- 127.0.0.3
- 127.0.0.4
- 127.0.0.5
- 127.0.0.6
- 127.0.0.7
- 127.0.0.9

> Keep in mind that each DNSBL has its own signals and you need to check the documentation of the DNSBL you are going to use, otherwise you may unintentionally block legit emails. For example `127.0.0.3` is a "listed" signal for Spamhaus, but not for others.

So, let's see how it works in practice. Let's check the `127.0.0.2` against `zen.spamhaus.org`:

```bash
$ dig 2.0.0.127.zen.spamhaus.org

; <<>> DiG 9.18.28 <<>> 2.0.0.127.zen.spamhaus.org
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 47788
;; flags: qr rd ra; QUERY: 1, ANSWER: 3, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;2.0.0.127.zen.spamhaus.org.	IN	A

;; ANSWER SECTION:
2.0.0.127.zen.spamhaus.org. 60	IN	A	127.0.0.2
2.0.0.127.zen.spamhaus.org. 60	IN	A	127.0.0.10
2.0.0.127.zen.spamhaus.org. 60	IN	A	127.0.0.4

;; Query time: 90 msec
;; SERVER: 45.90.28.0#53(45.90.28.0) (UDP)
;; WHEN: Tue Jul 30 12:51:56 EEST 2024
;; MSG SIZE  rcvd: 103
```

As you can see, the result contain `127.0.0.2` and `127.0.0.4` - both are "listed" signals for Spamhaus. So, if you receive a connection from that IP, you can safely reject the email with 4xx error.

But wait, why should you blindly trust a mere signal from an external service? Is there a way to know more?

Sure, just query TXT records of the same host (`2.0.0.127.zen.spamhaus.org`) for details:

```bash
$ dig txt 2.0.0.127.zen.spamhaus.org

; <<>> DiG 9.18.28 <<>> txt 2.0.0.127.zen.spamhaus.org
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 30365
;; flags: qr rd ra; QUERY: 1, ANSWER: 3, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;2.0.0.127.zen.spamhaus.org.	IN	TXT

;; ANSWER SECTION:
2.0.0.127.zen.spamhaus.org. 60	IN	TXT	"Listed by XBL, see https://check.spamhaus.org/query/ip/127.0.0.2"
2.0.0.127.zen.spamhaus.org. 60	IN	TXT	"Listed by SBL, see https://check.spamhaus.org/sbl/query/SBL2"
2.0.0.127.zen.spamhaus.org. 60	IN	TXT	"Listed by PBL, see https://check.spamhaus.org/query/ip/127.0.0.2"

;; Query time: 83 msec
;; SERVER: 45.90.28.0#53(45.90.28.0) (UDP)
;; WHEN: Tue Jul 30 12:54:30 EEST 2024
;; MSG SIZE  rcvd: 282
```

Spamhaus returned a few links where you can see human-readable explanations, here is a summary:

1. This IP or range has been observed to be involved in at least one of the following activities; sending spam, snowshoe spamming,hijacked IP space, or associated with bulletproof hosting.
2. The machine using this IP is infected with malware that is emitting spam, or is sharing a connection with an infected device. 
3. This IP is listed on the Policy Blocklist (the list of IPs that should not be sending email directly to the internet, maintained by ISPs)

**But is it enough?**

- To trust a single DNSBL? Of course not, you are supposed to query multiple DNSBLs at the same time, remember that list in the beginning of the post? Postmoogle uses _all of them_ to check the sender's IP, and makes a decision based on the results.

- To trust DNSBLs only? Nope. DNSBLs are just a part of the antispam system, you still need to implement SPF, DKIM, DMARC, and other voodoo to to get less spam in your inbox, just check the [previous posts](/categories/smtp/) in this blog.
