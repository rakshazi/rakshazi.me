+++
title = "Mandatory Email Headers or How to Send Email (into Spam)"
date = "2022-11-12T20:44:59+02:00"
author = "rakshazi"
cover = ""
categories = ["smtp"]
tags = ["smtp", "project::postmoogle"]
keywords = ["smtp", "email", "composing", "headers"]
description = "We already discussed topics like [SMTP components](/posts/smtp-components) and [email validation](/posts/how-to-validate-email-address/), so the next stop is how to actually compose an email message, including mandatory headers, to send a fully valid email straight into ~~inbox~~spam."
showFullContent = false
readingTime = false
hideComments = false
+++

Hey,
It's time to compose and send the first valid email message!

First of all, there are several mandatory headers and without any of them your email will be rejected:

* `MIME-Version` - defines "how to parse" the thing you sent. It has a hardcoded value of `1.0`
* `Content-Type` - defines "how to read" the email you sent. Usually, values are `text/html` or `text/plain` (and `charset="UTF-8"`, because we all ❤️ emojis)
* `Content-Transfer-Encoding` - kinda duplicate of `Content-Type`'s `charset`, so value `7bit` == ASCII and value `8bit` == Unicode
* `From` - defines the sender's email
* `To` - defines the recipient's email
* `Message-Id` - defines unique identification of that specific email, usually some random string +`@your-server.com`. Note the format: `<random-string@server-address>` (the `<` and `>` are mandatory)
* `Date` - defines "when it was sent" in RFC1123 with numeric zone format (go stdlib's `time.RFC1123Z` format: `Mon, 02 Jan 2006 15:04:05 -0700`)

Additionally, there are some headers, mandatory only in case of email threads (if you don't provide them an email will be valid, but an email thread - will not):

* `In-Reply-To` - defines `Message-Id` of the previous email in a thread
* `References` - an array of all `Message-Id`s in the thread


> Here is an example of a fully valid email

```yaml
MIME-Version: 1.0
Content-Type: text/plain; charset="UTF-8"
Content-Transfer-Encoding: 8BIT
From: your-email-sender@your-server.com
To: some@example.com
Message-Id: <some-random-string@your-server.com>
Date: Sat, 12 Nov 2022 19:13:27 +0000
Subject: A fully valid email example

That will go straight into the Spam dir of your mailbox!
```
