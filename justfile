# Shows help
default:
    @just --list --justfile {{ justfile() }}

# Update theme and 3rdparty resources
update:
    @cd themes/terminal; git pull

# Run locally
run:
    @xdg-open localhost:1313
    @hugo server -D

# Build (prod)
build:
    git submodule update --init --recursive
    @hugo --ignoreCache --minify

# Deploy (prod)
deploy:
    @echo "uploading website..."
    @bunny-upload -c ${BUNNY_CONFIG}
    @echo "done"
