+++
title = "SMTP components or Why sending emails is a rocket science"
date = "2022-11-10T15:39:41+02:00"
author = "rakshazi"
cover = ""
categories = ["smtp"]
tags = ["smtp", "project::postmoogle"]
keywords = ["smtp", "mua", "msa", "mta", "mda"]
description = "What are SMTP components and how SMTP servers implemented nowadays. Juggling with MUA, MSA, MTA, MDA and trying to understand why it's so hard to understand."
showFullContent = false
readingTime = false
hideComments = false
+++


Hey,
Wanna know the biggest scam you can get in the email world (apart from "email is a decentralized communication protocol")?

**Simple** Mail Transfer Protocol.

Simple, huh? They lied. It's damn rocket science!

I'm just trying to compile a note about what I found while working on [postmoogle](https://gitlab.com/etke.cc/postmoogle).

Let me just throw some smtp-related tags and see if you can correctly handle at least one of them: MUA, MSA, MTA, MDA.

Do you recall something? I bet you didn't. That's a description of the high-level flow on how email "Where is my order"
sent from your personal@email.com to support@amazon.com (of course, all addresses are just examples. Not sure if they even exist).

Digging through [Wikipedia](https://wikipedia.org):

* [MUA](https://en.wikipedia.org/wiki/Email_client) stands for `mail user agent`
* [MSA](https://en.wikipedia.org/wiki/Mail_submission_agent) stands for `mail submission agent`
* [MTA](https://en.wikipedia.org/wiki/Mail_transfer_agent) stands for `mail transfer agent`
* [MDA](https://en.wikipedia.org/wiki/Mail_delivery_agent) stands for `mail delivery agent`

> there are some other things, but they're a bit out of the scope of how SMTP works

Still don't get it? Ok, let me try to translate those M`*`As:

* **MUA** - email client (thunderbird, k-9 mail, apple mail)
* **MSA** - API endpoint where your email client sends data when you hit the "Send email" button. Input part of an "email server"
* **MTA** - a software to send emails received from users to other "email servers" (and their users). The output part of an "email server"
* **MDA** - reverse-MSA, a software that receives emails from other "email servers" and sends them to users (an "IMAP server input" thing)

So, how it works?

MUA (your mail client) sends a request to your email's MSA with the email you sent, and MSA forwards/delivers it to your recipient's MSA.

**Wait, where are all those MTA, MDA, M`*`A things?**

Well, they are part of the same "email server" program now.
Nowadays an "email server" can be only one program that runs with different input params:

```bash
$ emailserver --be-msa
$ emailserver --be-mta
$ emailserver --be-mda
# ...
```

And to understand that, you have to dig through Wikipedia, try to implement every component, understand that you have a horrifying mess of things, dig through Wikipedia again, catch zen and implement everything as 1 program even without `--be-X` params.

Yeah, that's how [postmoogle](/tags/projectpostmoogle) was implemented - I didn't have a clue about how email actually works.

Now I have :D
