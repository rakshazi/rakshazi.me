+++
title = "DNS, rDNS, FCrDNS"
date = "2022-11-24T23:22:13+02:00"
author = "rakshazi"
cover = ""
categories = ["smtp"]
tags = ["smtp", "project::postmoogle"]
keywords = ["smtp", "email", "antispam"]
description = "A new mindblowing discovery I made during the work on [Postmoogle](https://gitlab.com/etke.cc/postmoogle), but this time the finding will stay as a blog post only and won't be actually implemented, because it's a Pandora's box of email."
showFullContent = false
readingTime = false
hideComments = false
+++

> Sup dawg,
> I heard you like DNS, so I put DNS into your DNS so you can query DNS while you querying DNS.
>
> -- Tibizx

Yeah, memes from 2008 to explain [things from 1996](https://www.rfc-editor.org/rfc/rfc1912).

Anyway, to the topic: what does those acronyms stand for?

## DNS

Domain Name System = convert website domain in the browser's address bar to the actual server's IP where that website hosted.
It's a common thing, so let's move to the next one.

## rDNS

Stands for **reverse** DNS. If `DNS` (or `forward DNS`) used when you want to convert domain to IP, the rDNS does exact opposite - it converts IP into domain. For all of you who knows what DNS is, but never touched rDNS topic and now have the only question:

### How?

Apparently, the "forward" DNS and the reverse DNS are different branches of the same DNS thing.
The difference? `.in-addr.arpa` and `ip6.arpa` domains. Any IP address is subdomain of one of those subdomains (the former is for IPv4, the latter is for IPv6). So, any IP address can be reversed (`reverse` DNS, you know) and represented as subdomain of `in-addr.arpa`.

_For the sake of simplicity I'll just use IPv4 as example_

If we have an IP `123.123.123.123`, the PTR record for that IP would be `321.321.321.321.in-addr.arpa`.

Want some real example?

```bash
$ dig -x 8.8.4.4 # google public dns
4.4.8.8.in-addr.arpa.	2685	IN	PTR	dns.google.
```

Okay, why would you need it?
Actually, you don't. It's still used, tho, and you may already guessed the right answer - in emails! Email filtering software like rspamd, SpamAssasin, etc., actually use the rDNS and PTR records to determine the spam score for each email message and they try to lookup reverse DNS record(-s) of the sender's domain.

Yes, another anti-spam measurement. I know, email is 99% about anti-spam and only 1% about meaningful data.

So, how spam filters determine if a message sent from definitely.not.spam@dns.google is legit? By performing thousands of different checks on-the-fly, of course. But today we'll talk about one specific validation, in fact, it's almost olbsoleted by SPF, but still used in the wild.
And the name is...

## FCrDNS

or Forward Confirmed reverse DNS. That part is pretty simple - you just get forward DNS lookup and reverse DNS lookup and match them.

Yeah, that simple.

```bash
$ dig dns.google # forward DNS lookup
dns.google.		285	IN	A	8.8.4.4

$ dig -x 8.8.4.4 # reverse DNS lookup
4.4.8.8.in-addr.arpa.	2685	IN	PTR	dns.google.
```

If you can get domain name from the rDNS lookup using the IP address you got from DNS lookup - sender looks legit! A bit.
Email will be thrown in spam, anyway.

### Why?

Because there were no SPF in 1996. In fact, FCrDNS is a "granddaddy" of SPF, dead simple, without any features like redirects or includes.

## Conclusion

I don't have one this time. For me it was a revelation that reverse DNS is a thing and how exactly it works (of course I heard about rDNS before and even touched it a bit, but never **used** it), especially when I found out about the FCrDNS technique (IIRC, from rspamd rules) and that it's actually alive, but with very low impact on your spam score.
